package fr.info.sdweb;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.boutique.ejb.categorieEjbRemote;
import fr.boutique.model.CategorieBean;

/**
 * Servlet implementation class CategorieServlet
 */
@WebServlet("/Categorie")
public class CategorieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategorieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//request.getRequestDispatcher("Categorie.jsp").forward(request, response);
		
		List<CategorieBean> list = new ArrayList<CategorieBean>();
		HttpSession session = request.getSession(true);
		//Connexion JNDI pour localiser l'EJB
		try {
			final Hashtable jndiProperties = new Hashtable();
			jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
			final Context context = new InitialContext(jndiProperties);
			final String appName = "BoutiqueEAR";
			final String moduleName = "BoutiqueEJB";
			final String beanName = "categorieEjb";
			final String viewClassName = categorieEjbRemote.class.getName();
			
			categorieEjbRemote remote = (categorieEjbRemote)context.lookup("ejb:"+appName+"/"+moduleName+"/"+beanName+"/"+viewClassName);
			list = remote.getCategories();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		session.setAttribute("BeanCategorie", list);
		response.sendRedirect("Categorie.jsp");
		System.out.println(list.get(1).getNom());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
