<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.ArrayList" %>
  <%@ page import = "fr.boutique.model.*" %>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Categorie de produit</title>
</head>
	<body>
	
		<div id="entete">
			<h1>Categorie de produit</h1>
		</div>
		<div id="Menu">
			<h3>Menu</h3>
			<ul>
				<li><a href="<%=request.getContextPath()+"/Accueil"%>">Accueil</a></li>
				<li><a href="<%=request.getContextPath()+"/Categorie"%>">Categories</a></li>
			</ul>
		</div>
		<div id="corp">
			<p>
				Catégorie ! <br />
				<c:forEach items="${BeanCategorie}" var="categorie">
					<c:out value="${categorie}" />
				</c:forEach>
			</p>
		</div>
	
	</body>
</html>