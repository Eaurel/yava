package fr.boutique.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="Catalogue", name="Commande_client")
public class CommandeClientBean implements Serializable {
	
	private int id;
	private double montant;
	private String date;
	private int noComfirmation;
	private int clientId;
	
	
	@Id
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="montant")
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	
	@Column(name="date")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	@Column(name="no_confirmation")
	public int getNoComfirmation() {
		return noComfirmation;
	}
	public void setNoComfirmation(int noComfirmation) {
		this.noComfirmation = noComfirmation;
	}
	
	@Column(name="client_id")
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

}
