package fr.boutique.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="Catalogue", name="Produit_Commande")
public class ProduitCommandeBean implements Serializable {
	
	private int commandeId;
	private int produitId;
	private int quantite;
	
	@Id
	@Column(name="commande_client_id")
	public int getCommandeId() {
		return commandeId;
	}
	public void setCommandeId(int commandeId) {
		this.commandeId = commandeId;
	}
	
	@Id
	@Column(name="produit_id")
	public int getProduitId() {
		return produitId;
	}
	public void setProduitId(int produitId) {
		this.produitId = produitId;
	}

	@Column(name="quantite")
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	
}
