package fr.boutique.ejb;

import java.util.List;

import javax.ejb.Remote;

import fr.boutique.model.CategorieBean;

@Remote
public interface categorieEjbRemote {
	public List<CategorieBean> getCategories();
}
