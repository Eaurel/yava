package fr.boutique.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.boutique.model.CategorieBean;

/**
 * Session Bean implementation class categorieEjb
 */
@Stateless(name="BoutiqueJNDI")
public class categorieEjb {

	@PersistenceContext(unitName="managerBoutique")
	EntityManager mh;
    /**
     * Default constructor. 
     */
    public categorieEjb() {
        // TODO Auto-generated constructor stub
    }
    
    public List<CategorieBean> getCategories(){
    	Query q = mh.createQuery("select h.nom from CategorieBean h");
    	List<CategorieBean> list = q.getResultList();
    	return list;
    }

}
