package fr.ulco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Session Bean implementation class HelloBean
 */

@Entity
@Table(schema="hellodata", name="HelloTable")
public class HelloBean implements Serializable {

	private int id;
	private String aName = "";
	
	@Id
	@GeneratedValue
	@Column(name="pk_hello")
	
	public int getId(){
	return id;
	}
	
	public void setId(int id){
	this.id = id;
	}
	
	@Column(name="message")
    public String getName()
    {
    	return this.aName;
    }
    
    public void setName(String pName)
    {
    	this.aName = pName;
    }

}
