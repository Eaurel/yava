package fr.ulco.ejb.Catalogue;

import java.util.ArrayList;

public interface accesCatalogueBeanRemote {
	public ArrayList<Categorie> getListCategories();
	public ArrayList<Produit> getListProduits(int id);
}
