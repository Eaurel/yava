package fr.info.sdweb;

import java.io.IOException;
import java.util.Collection;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.ulco.model.HelloBean;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/Hello")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		
		//String surname = request.getParameter("surname");
		//request.setAttribute("s", surname);
		String name = request.getParameter("name");
		//request.setAttribute("n", name);
		//request.getRequestDispatcher("Hello.jsp").forward(request, response);
		
		/*HelloBean bean = new HelloBean();
		bean.setName(name);
		request.setAttribute("beanHello", bean);
		request.getRequestDispatcher("Hello2.jsp").forward(request, response);
		System.out.println("je continue a travailler " + name);
		*/
		
		Collection<HelloBean> listHello = new Vector<HelloBean>();
		for (int i = 0; i < 10; i++) {
			HelloBean bean = new HelloBean();
			bean.setName(name + " " + i);
			listHello.add(bean);
		}
		session.setAttribute("beanHello", listHello);
		
		response.sendRedirect("Hello3.jsp");
		System.out.println("je continue a travailler pour Mister " + name);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
