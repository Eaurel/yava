<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hello <%=request.getParameter("name") %></title>
</head>
<body>
	<%
	String name = (String)request.getAttribute("n");
	String surname = (String)request.getAttribute("s");
	String fullname = name + " " + surname;
	%>
	My name is <%=request.getParameter("surname") %>
	, <%= fullname %>
	<br/>
	<a href="/HelloApp">Back</a>
</body>
</html>