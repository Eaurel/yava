package fr.ulco.ejb.Commande;

@Entity
@Table(schema="boutique", name="commandes")
public class Commande implements Serializable{
	private int id;
	private String numero;
	private Collection<LigneCommande> lignesCommandes;
	
	@Id
	@Column(name="pk_commande")
	@GeneratedValue
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="numero")
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	@OneToMany(targetEntity=LigneCommande.class, mappedBy="commande",cascade=CascadeType.ALL)
	public Collection<LigneCommande> getLignesCommandes() {
		return lignesCommandes;
	}
	
	public void setLignesCommandes(Collection<LigneCommande> lignesCommandes) {
		this.lignesCommandes = lignesCommandes;
	}
	
	public String toString(){
		return "id "+ id + " numero " + numero;
	}
}
